from gi.repository import Gtk
global Answer,S
class Handler:
    def onDeleteWindow(self, *args):
        Gtk.main_quit(*args)
    def onNumb1Pressed(self,button):
        Entry1.set_text(str(Entry1.get_text()+"1"))
    def onNumb2Pressed(self,button):
       Entry1.set_text(str(Entry1.get_text()+"2"))
    def onNumb3Pressed(self,button):
       Entry1.set_text(str(Entry1.get_text()+"3"))
    def onNumb4Pressed(self,button):
       Entry1.set_text(str(Entry1.get_text()+"4"))
    def onNumb5Pressed(self,button):
       Entry1.set_text(str(Entry1.get_text()+"5"))
    def onNumb6Pressed(self,button):
       Entry1.set_text(str(Entry1.get_text()+"6"))
    def onNumb7Pressed(self,button):
       Entry1.set_text(str(Entry1.get_text()+"7"))
    def onNumb8Pressed(self,button):
       Entry1.set_text(str(Entry1.get_text()+"8"))
    def onNumb9Pressed(self,button):
       Entry1.set_text(str(Entry1.get_text()+"9"))
    def onNumb0Pressed(self,button):
       Entry1.set_text(str(Entry1.get_text()+"0"))  
    def onNumb00Pressed(self,button):
       Entry1.set_text(str(Entry1.get_text()+"."))
    def onAmalJamPressed(self,button):
       Entry1.set_text(str(Entry1.get_text()+"+")) 
    def onAmalTaghsimPressed(self,button):
       Entry1.set_text(str(Entry1.get_text()+"/"))
    def onAmalTafrighPressed(self,button):
       Entry1.set_text(str(Entry1.get_text()+"-"))
    def onAmalZarbPressed(self,button):
       Entry1.set_text(str(Entry1.get_text()+"*")) 
    def onAmalMosaviPressed(self,button):
       S=Entry1.get_text()
       S.split()
       if ("+" in S ):
           Nu1=float(S[0:S.index("+")])
           Nu2=float(S[S.index("+")+1:len(S)])
           print("Nu1:",Nu1)
           print("Nu2:",Nu2)
           Answer=Nu1+Nu2
       if ("-" in S):
           Nu1=float(S[0:S.index("-")])
           Nu2=float(S[S.index("-")+1:len(S)])
           print("Nu1:",Nu1)
           print("Nu2:",Nu2)
           Answer=Nu1-Nu2
       if ("*" in S ):
           Nu1=float(S[0:S.index("*")])
           Nu2=float(S[S.index("*")+1:len(S)])
           print("Nu1:",Nu1)
           print("Nu2:",Nu2)
           Answer=Nu1*Nu2
       if ("/" in S ):
           Nu1=int(S[0:S.index("/")])
           Nu2=int(S[S.index("/")+1:len(S)])
           print("Nu1:",Nu1)
           print("Nu2:",Nu2)
           Answer=Nu1/Nu2
       print(Answer)
       Entry1.set_text(str(Answer))
    def onAmalEntryPressed(self,button):
       pass

builder = Gtk.Builder()
builder.add_from_file("GUI.glade")
builder.connect_signals(Handler())
window = builder.get_object("window1")
window.show_all()
window.set_title(" حسابگر v0.1 alpha1")

Numb1=builder.get_object("button1")
Numb2=builder.get_object("button2")
Numb3=builder.get_object("button3")
Numb4=builder.get_object("button5")
Numb5=builder.get_object("button6")
Numb6=builder.get_object("button7")
Numb7=builder.get_object("button9")
Numb8=builder.get_object("button10")
Numb9=builder.get_object("button11")
Numb0=builder.get_object("button13")
Numb00=builder.get_object("button14")
AmalJazr=builder.get_object("button18")
AmalTavan=builder.get_object("button19")
AmalCls=builder.get_object("button17")
AmalZarb=builder.get_object("button12")
AmalTaghsim=builder.get_object("button16")
AmalJam=builder.get_object("button4")
AmalTafrigh=builder.get_object("button8")
AmalMosavi=builder.get_object("button15")
AmalMosavi.set_label("_=")
AmalMosavi.set_use_underline(True)
Entry1=builder.get_object("entry1")
Entry1.set_max_length(20)

Gtk.main()
